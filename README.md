# Emacs configuration and hacks

These settings work on Emacs 23. I haven't tested them elsewhere. Most added
features and behaviors replicate those I found (and liked) in other editors or
IDE's like Eclipse and Visual Studio. The bulk of these customizations are
included in three libraries:

* `workspace.el' -- Define a workspace dir to confine your searches
* `ifind.el' -- Find a file interactively within the workspace
* `rotbuf.el' -- Use ctrl-tab to switch buffers in a conventional way

The rest are small hacks that don't really need a package of their own. They are
included in `init.el'.

The Perl and PHP sections of this configuration file assume these packages are
installed:

* php-elisp
* perlnow.el
* template.el
