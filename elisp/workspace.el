;;; workspace.el --- Confine your file searches to a customizable directory

;; Installation:
;;
;; 1. Copy this file into any directory in your `load-path'
;; 2. Add this line to your Emacs config file:
;;      (require 'workspace)

;; Usage:
;;
;; Customize the variable `workspace-dir'.
;; Find files in workspace with the command M-x workspace-find
;; Grep files in workspace with the command M-x workspace-grep
;; (You can also use the aliases wf and wg respectively.)
;; If you create a TAGS file at the root of the workspace, it will
;; always get loaded on startup.

(provide 'workspace)

(defcustom workspace-dir "~/workspace"
  "The workspace directory. File searches will be performed within it."
  :group 'local
  :type 'string)

(defun workspace-grep (pattern)
  "Invoke `rgrep' on all files of `wokspace-dir' for the given PATTERN."
  (interactive "sGrep pattern: ")
  (rgrep pattern "*" workspace-dir))

(defun workspace-find (pattern)
  "Invoke `find-name-dired' on `workspace-dir' for the given PATTERN."
  (interactive "sFind file: ")
  (find-name-dired workspace-dir pattern))

(defalias 'wf 'workspace-find)
(defalias 'wg 'workspace-grep)

;; This initialization is used by `rgrep'
(grep-compute-defaults)

;; Load TAGS for this workspace, if the file exists
(let ((tags-file (concat workspace-dir "/TAGS")))
  (when (file-exists-p tags-file)
    (visit-tags-table tags-file)))

;;; workspace.el ends here
