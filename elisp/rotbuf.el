;;; rotbuf.el -- Rotate/switch buffers in a conventional way

;; Make Emacs buffer switching behave pretty much like in any
;; application written in the present century :|

;; Installation:
;;
;; 1. Copy this file into any directory in your `load-path'
;; 2. Add this line to your Emacs config file:
;;      (require 'rotbuf)
;;
;; Then set your favorite keyboard shortcut to `rotbuf-switch':
;;      (global-set-key [(control tab)] 'rotbuf-switch)

(provide 'rotbuf)

(defvar rotbuf-repetitions 0
  "Number of times `rotbuf-switch' was repeated.")

(defvar rotbuf-list ()
  "List of non-special buffers open.")

(defun rotbuf-switch ()
  "Switch to buffer in my buffer list.
You should bind this function to Ctrl-Tab or something."
  (interactive)
  ;; if the last command wasn't a switch buffer, reset
  (when (not (eq last-command 'rotbuf-switch))
    (setq rotbuf-repetitions 0
          rotbuf-list (rotbuf-filter-buffers 'rotbuf-normal-buffer-p)))
  ;; if the current buffer is not a special buffer
  (when (not (rotbuf-special-buffer-p (current-buffer)))
    (setq rotbuf-repetitions (1+ rotbuf-repetitions))
    ;; swap or rotate
    (if (< rotbuf-repetitions (length rotbuf-list))
        (setq rotbuf-list (rotbuf-swap rotbuf-list 0 rotbuf-repetitions))
      (setq rotbuf-list (rotbuf-rotate rotbuf-list))
      (setq rotbuf-repetitions 0)))
  ;; switch to 1st buffer
  (switch-to-buffer (elt rotbuf-list 0))
  (rotbuf-reorder-buffer-list
   (append rotbuf-list
		   (rotbuf-filter-buffers 'rotbuf-special-buffer-p))))

(defun rotbuf-filter-buffers (filter-function)
  "Returns a list of buffers that match FILTER-FUNCTION."
  (delq nil
        (mapcar (lambda (buffer)
                  (if (funcall filter-function buffer) buffer nil))
                (buffer-list))))

(defun rotbuf-special-buffer-p (buffer)
  "Returns t if BUFFER is one of the special buffers, `nil' otherwise.
A special buffer is one whose name starts with an asterisk. And `TAGS'."
  (let ((name (buffer-name buffer)))
    (or (string-match "^ ?\\*" name)
        (equal "TAGS" name))))

(defun rotbuf-normal-buffer-p (buffer)
  "This is the complement of `rotbuf-special-buffer-p'."
  (not (rotbuf-special-buffer-p buffer)))

(defun rotbuf-reorder-buffer-list (new-list)
  "Reorder buffer list using NEW-LIST."
  (while new-list
    (bury-buffer (car new-list))
    (setq new-list (cdr new-list))))

(defun rotbuf-swap (the-list i j)
  "Swap I and J elements in THE-LIST."
  (let ((tmp (nth j the-list))
        (vec (vconcat the-list)))
    (aset vec i tmp)
    (aset vec j (nth i the-list))
    (append vec nil)))

(defun rotbuf-rotate (the-list)
  "Delete first elem in THE-LIST and append it to the end."
  (append (cdr the-list) (list (car the-list))))

;;; rotbuf.el ends here