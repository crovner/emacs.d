;;; Emacs init file for Christian Rovner            -*- coding: utf-8 -*-
;; ----------------------------------------------------------------------

(require 'workspace)
(require 'ifind)
(require 'rotbuf)

(defun search-at-point ()
  "Highlight word at point by starting an isearch on that word."
  (interactive)
  (isearch-mode nil)
  (let ((word (symbol-name (symbol-at-point))))
    (setq isearch-string word
          isearch-message word))
  (isearch-search-and-update))

(defun perltidy-region ()
  "Run perltidy on the active region (or the whole buffer by default)."
  (interactive)
  (save-excursion
    (let ((beg (if mark-active (point) (point-min)))
          (end (if mark-active (mark) (point-max))))
      (shell-command-on-region beg end "perltidy -q" nil t))))

(defun expand-region-to-whole-lines ()
  "Expand the region to make it encompass whole lines.
If no region is selected, select the current line."
  (if (not mark-active)
      ;; Create region from current line
      (progn
        (beginning-of-line)
        (set-mark (point))
        (end-of-line))
    ;; The mark is active, expand region
    (let ((beg (region-beginning))
          (end (region-end)))
      (goto-char beg)
      (beginning-of-line)
      (set-mark (point))
      (goto-char end)
      (unless (bolp) (end-of-line)))))

(defun my-comment-region ()
  "Comment or uncomment region after expanding it to whole lines."
  (interactive)
  (save-excursion
    (expand-region-to-whole-lines)
    (comment-or-uncomment-region (region-beginning) (region-end))))

(defun my-increase-left-margin ()
  "Increase left margin in region after expanding it to whole lines."
  (interactive)
  (let (deactivate-mark)
    (expand-region-to-whole-lines)
    (increase-left-margin (region-beginning) (region-end) nil)))

(defun my-decrease-left-margin ()
  "Decrease left margin in region after expanding it to whole lines."
  (interactive)
  (let (deactivate-mark)
    (expand-region-to-whole-lines)
    (decrease-left-margin (region-beginning) (region-end) nil)))

(defun toggle-tab-width ()
  "Toggle variable tab-width between 8 and 4."
  (interactive)
  (set-variable 'tab-width (if (= tab-width 8) 4 8))
  (message "Tab width set to %d" tab-width))

(defun toggle-tabs-mode ()
  "Toggle variable indent-tabs-mode between t and nil."
  (interactive)
  (set-variable 'indent-tabs-mode (not indent-tabs-mode))
  (message "Tabs mode set to %s" indent-tabs-mode))

(defun unfill-region (begin end)
  "Remove all linebreaks in a region.
Except paragraphs, indented text and lines starting with an asterix."
  (interactive "r")
  (replace-regexp "\\([^\n]\\)\n\\([^ *\n]\\)" "\\1 \\2" nil begin end))

(defun smart-tab ()
  "When pressing tab, autocomplete or indent according to context."
  (interactive)
  (if (minibufferp)
      (minibuffer-complete)
    (if mark-active
        (indent-region (region-beginning)
                       (region-end))
      (if (looking-at "\\_>")
          (dabbrev-expand nil)
        (indent-for-tab-command)))))

(dolist (mode '(fundamental text cperl php ruby python js2 sql c++ html css emacs-lisp lisp-interaction))
  (add-hook (intern (concat (symbol-name mode) "-mode-hook"))
            (lambda () (local-set-key [tab] 'smart-tab))))

;; No me rompas las pelotas
(fset 'yes-or-no-p 'y-or-n-p)

;; ----------------------------------------------------------------------
;; Preferences

(set-cursor-color "#cccccc")

;; Keyboard shortcuts
(global-set-key [(control .)]            'search-at-point)
(global-set-key [(control tab)]          'rotbuf-switch)
(global-set-key [(control ?')]           'my-comment-region)
(global-set-key [(control >)]            'my-increase-left-margin)
(global-set-key [(control <)]            'my-decrease-left-margin)
(global-set-key [(meta ?ñ)]              'ediff-buffers)
(global-set-key [(control meta _)]       'revert-buffer)
(global-set-key [(control shift d)]      'kill-whole-line)
(global-set-key [f6]                     'magit-status)
(global-set-key [f7]                     'find-variable-at-point)
(global-set-key [f8]                     'find-function-at-point)
(global-set-key [(control insert)]       'clipboard-kill-ring-save)
(global-set-key [(shift insert)]         'clipboard-yank)
(global-set-key [(control meta t)]       'toggle-tabs-mode)
(global-set-key [(control meta w)]       'toggle-tab-width)

;; Interactively do things
(require 'ido)
(add-to-list 'ido-work-directory-list-ignore-regexps tramp-file-name-regexp)
(add-hook 'ido-minibuffer-setup-hook
           (lambda ()
             (make-local-variable 'truncate-lines)
             (setq truncate-lines nil)))
(ido-mode t)

;; Perl
(defalias 'perl-mode 'cperl-mode)
(add-hook 'cperl-mode-hook
          (lambda ()
            (require 'template)
            (template-initialize)
            (require 'perlnow)
            (local-set-key [f5] 'perlnow-run)
            (cperl-set-style "BSD")))

;; PHP
(add-hook 'php-mode-hook
          (lambda ()
            (set-variable 'indent-tabs-mode nil)
            (c-set-offset 'class-open 0)
            (c-set-offset 'inline-open 0)
            (c-set-offset 'substatement-open 0)
            (setq c-default-style "bsd"
                  c-basic-offset 4
                  comment-start "//"
                  comment-end "")))

;; CSS
(setq cssm-indent-function #'cssm-c-style-indenter)
(setq cssm-indent-level '2)

;; File types
(setq auto-mode-alist
      (append '(
                ("\\.tpl$"      . html-mode)
                ("\\.template$" . html-mode)
                ("\\.frag$"     . html-mode)
                ("\\.mako$"     . html-mode)
                ("\\.as[pc]x$"  . html-mode)
                ("\\.rake$"     . ruby-mode)
                ("Rakefile$"    . ruby-mode)
                ("Gemfile$"     . ruby-mode)
                ("Guardfile$"   . ruby-mode)
                ("\\.haml$"     . haml-mode)
                ("\\.sass$"     . sass-mode)
                ("\\.coffee$"   . coffee-mode)
                ("Cakefile$"    . coffee-mode)
                ("\\.bash_"     . shell-script-mode)
               ) auto-mode-alist))

;; ----------------------------------------------------------------------
;; Added by Emacs

(custom-set-variables
  ;; custom-set-variables was added by Custom.
  ;; If you edit it by hand, you could mess it up, so be careful.
  ;; Your init file should contain only one such instance.
  ;; If there is more than one, they won't work right.
 '(auto-save-file-name-transforms (quote ((".*" "~/.emacs.d/tmp" t))))
 '(backup-directory-alist (quote (("." . "~/.emacs.d/backups"))))
 '(browse-url-browser-function (quote browse-url-default-browser))
 '(c-default-style (quote ((c++-mode . "stroustrup") (java-mode . "java") (awk-mode . "awk") (other . "bsd"))))
 '(cua-mode t nil (cua-base))
 '(cua-paste-pop-rotate-temporarily t)
 '(ediff-split-window-function (quote split-window-horizontally))
 '(fill-column 80)
 '(global-font-lock-mode t)
 '(grep-find-ignored-directories (quote ("SCCS" "RCS" "CVS" "MCVS" ".svn" ".git" ".hg" ".bzr" "_MTN" "_darcs" "{arch}" "log" "var" "tmp")))
 '(grep-find-template "find . <X> -not -name TAGS -type f <F> -print0 | xargs -0 -e grep <C> -nH -e <R>")
 '(haskell-check-command "ghc -fno-code")
 '(ido-enable-tramp-completion nil)
 '(ido-ignore-files (quote ("\\`CVS/" "\\`#" "\\`.#" "\\`\\.\\./" "\\`\\./" "^/sudo:")))
 '(ido-max-prospects 20)
 '(ido-max-window-height nil)
 '(ido-max-work-file-list 30)
 '(ido-record-ftp-work-directories nil)
 '(ido-separator " | ")
 '(indent-tabs-mode nil)
 '(inhibit-startup-screen t)
 '(js2-basic-offset 4)
 '(js2-highlight-level 2)
 '(max-mini-window-height 0.4)
 '(menu-bar-mode nil)
 '(save-place t nil (saveplace))
 '(save-place-file "~/.emacs.d/saved-places")
 '(scroll-bar-mode nil)
 '(show-paren-mode t)
 '(tab-width 4)
 '(tool-bar-mode nil)
 '(tramp-auto-save-directory "~/.emacs.d/backups")
 '(tramp-backup-directory-alist (quote (("." . "~/.emacs.d/backups"))))
 '(tramp-verbose 2)
 '(truncate-lines t)
 '(user-mail-address "crovner@gmail.com")
 '(vc-annotate-very-old-color "#3F3FAF")
 '(visible-bell t))

(custom-set-faces
  ;; custom-set-faces was added by Custom.
  ;; If you edit it by hand, you could mess it up, so be careful.
  ;; Your init file should contain only one such instance.
  ;; If there is more than one, they won't work right.
 '(default ((t (:inherit nil :stipple nil :background "black" :foreground "white" :inverse-video nil :box nil :strike-through nil :overline nil :underline nil :slant normal :weight normal :width normal :foundry "unknown" :family "DejaVu Sans Mono"))))
 '(mode-line ((((class color) (min-colors 88)) (:background "grey10" :foreground "white"))))
 '(mode-line-inactive ((default (:inherit mode-line)) (((class color) (min-colors 88) (background dark)) (:background "grey10" :foreground "grey50")))))
